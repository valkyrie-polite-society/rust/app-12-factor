use serde::*;
use lapin::{Connection, ConnectionProperties};
use mongodb::Client;
use config::{Config, ConfigError, File};
use std::path::PathBuf;
use crate::errors::AppError;

#[derive(Deserialize, Debug)]
pub struct DatabaseConfig {
  mongodb_connection_uri: String,
}

pub fn get_database_config() -> Result<DatabaseConfig, ConfigError> {
  let location = std::env::var("DATABASE_CONFIG_FILE").unwrap_or_else(|_| "config/database.yaml".into());
  let file = File::from(PathBuf::from(location));
  let mut config = Config::new();
  config.merge(file)?;
  config.try_into()
}

pub async fn get_database_client() -> Result<Client, AppError> {
  let config = get_database_config()?;

  return Client::with_uri_str(&config.mongodb_connection_uri)
    .await
    .map_err(|e| AppError::DatabaseError(e));
}

#[derive(Deserialize, Debug)]
pub struct CacheConfig {
  redis_connection_uri: String,
}

fn get_cache_config() -> Result<CacheConfig, ConfigError> {
  let location = std::env::var("CACHE_CONFIG_FILE").unwrap_or_else(|_| "config/cache.yaml".into());
  let file = File::from(PathBuf::from(location));
  let mut config = Config::new();
  config.merge(file)?;
  config.try_into()
}

pub fn get_cache_client() -> Result<redis::Client, AppError> {
  let config = get_cache_config()?;

  redis::Client::open(config.redis_connection_uri)
    .map_err(|e| AppError::CacheError(e))
}

#[derive(Deserialize, Debug)]
pub struct MessageQueueConfig {
  rabbitmq_connection_uri: String,
}

fn get_message_queue_config() -> Result<MessageQueueConfig, ConfigError> {
  let location = std::env::var("MESSAGE_QUEUE_CONFIG_FILE").unwrap_or_else(|_| "config/message-queue.yaml".into());
  let file = File::from(PathBuf::from(location));
  let mut config = Config::new();
  config.merge(file)?;
  config.try_into()
}

pub async fn get_message_queue_connection() -> Result<Connection, AppError> {
  let config = get_message_queue_config()?;

  Connection::connect(
    &config.rabbitmq_connection_uri,
    ConnectionProperties::default().with_default_executor(8),
  ).await
    .map_err(|e| AppError::MessageQueueError(e))
}
