#![feature(async_closure)]

use crate::errors::AppError;
use crate::config::*;
use lapin::BasicProperties;
use lapin::options::{BasicPublishOptions, QueueDeclareOptions};
use lapin::types::FieldTable;
use mongodb::Client;
use redis::AsyncCommands;
use std::future::Future;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::sync::Arc;
use tokio::signal::unix::{signal, SignalKind};
use tokio::sync::{oneshot, oneshot::Receiver};
use warp::Filter;

mod errors;
mod config;

async fn get_mongodb_client() -> Arc<Client> {
  Arc::new(get_database_client().await.unwrap())
}

async fn with_mongodb_client(client: impl Future<Output=Arc<Client>>) -> Result<warp::reply::Json, warp::reject::Rejection> {
  let client = client.await;

  client.list_database_names(None, None)
    .await
    .map(|names| warp::reply::json(&names))
    .map_err(|err| warp::reject::custom(AppError::DatabaseError(err)))
}

#[tokio::main]
async fn main() -> Result<(), AppError> {
  // Warp
  let with_mongodb = warp::any().map(get_mongodb_client);

  let mongodb_connection_test = warp::path!("mongodb" / "can-connect")
    .and(with_mongodb)
    .and_then(with_mongodb_client);

  let health = warp::path!("healthz")
    .map(health_route);

  let routes = health.or(mongodb_connection_test);

  let (sender, receiver) = oneshot::channel::<()>();

  let mut sigint = signal(SignalKind::interrupt()).unwrap();
  let mut sigterm = signal(SignalKind::terminate()).unwrap();

  let listen_address = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), 42069);
  let (_addr, server) = warp::serve(routes)
    .bind_with_graceful_shutdown(listen_address, warp_shutdown(receiver));

  let _ = tokio::join!(
        tokio::spawn(server),
        tokio::spawn(async move {
            tokio::select! {
              Some(_) = sigint.recv() => {
                  sender.send(()).unwrap();
              }
              Some(_) = sigterm.recv() => {
                  sender.send(()).unwrap();
              }
            }
        }));

  /*
  // Redis
  let redis_client = get_cache_client()?;
  let mut con = redis_client.get_async_connection().await?;
  con.set("a", "4").await?;

  // RabbitMQ
  let conn = get_message_queue_connection().await?;
  let channel_a = conn.create_channel().await?;
  channel_a
    .queue_declare(
      "hello",
      QueueDeclareOptions::default(),
      FieldTable::default(),
    )
    .await?;

  channel_a
    .basic_publish(
      "",
      "hello",
      BasicPublishOptions::default(),
      b"Hello world!".to_vec(),
      BasicProperties::default(),
    ).await?
    .await?;*/

  Ok(())
}

async fn warp_shutdown(receiver: Receiver<()>) {
  println!("Server started");
  receiver.await.ok();
  println!("Gracefully shutting down");
}

fn health_route() -> String {
  String::from("Ok")
}
