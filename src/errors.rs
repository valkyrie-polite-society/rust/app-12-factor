#[derive(thiserror::Error, Debug)]
pub enum AppError {
  #[error("configuration error")]
  ConfigurationError(#[from] config::ConfigError),
  #[error("database error")]
  DatabaseError(#[from] mongodb::error::Error),
  #[error("cache error")]
  CacheError(#[from] redis::RedisError),
  #[error("message queue error")]
  MessageQueueError(#[from] lapin::Error)
}

use warp::reject::Reject;

impl Reject for AppError {}
